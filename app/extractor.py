from typing import *
from dataclasses import dataclass
from datetime import datetime
import logging
import jsons

file_path = './My Clippings.txt'


@dataclass
class Note:
    id: int
    title: str
    content: str
    type_note: str
    author: str
    date: str
    tags: List[str]
    metadata: Dict[str, str]


def parse_date_from(literal: str) -> datetime:
    cleaned = literal.split(',')[1].strip()

    day, month, year, time = cleaned.split(' ')
    hour, minutes, seconds = time.split(':')

    map_month = {
        'January': 1,
        'February': 2,
        'March': 3,
        'April': 4,
        'May': 5,
        'June': 6,
        'July': 7,
        'August': 8,
        'September': 9,
        'October': 10,
        'November': 11,
        'December': 12,

    }

    parsed_date = datetime(int(year), map_month[month], int(
        day), int(hour), int(minutes), int(seconds))

    return parsed_date


def get_clippings_from_path(path: str = file_path) -> List[Note]:
    list_notes: List[Note] = []
    metadata: Dict[str, str] = {
        'page': '',
        'location': '',
    }

    with open(file_path) as f:
        list_notes: List[Note] = []
        append = list_notes.append

        id = 1
        while f.readable():
            note_block: List[str] = []
            while True:
                line = f.readline()
                if line == '' or line.startswith('====='):
                    break

                line.strip('\n').strip()
                note_block.append(line)

            if len(note_block) < 3:
                break

            head = note_block[0]
            title = head
            if (ind := head.rfind('(')) > -1:
                title = head[:ind]
                author = head[ind + 1:].strip(')\n')
            else:
                author = ''

            date = None

            # Extract medatata
            meta_l: List[str] = note_block[1][1:].split(' | ')
            meta_l = [s.strip() for s in meta_l]
            if len(meta_l) == 3:
                metadata['page'] = meta_l[0]
                metadata['location'] = meta_l[1]
                date = parse_date_from(meta_l[2])
            elif len(meta_l) == 2:
                metadata['location'] = meta_l[0]
                date = parse_date_from(meta_l[1])
            else:
                logging.error(f'Cannot extract metadata, got: {meta_l}')

            clip = ''.join(note_block[3:]).replace('.', '.\n').strip('\n')

            date_format = '%Y-%m-%d %H:%M:%S'
            if date is None:
                literal_date = datetime.now().strftime(date_format)
            else:
                literal_date = date.strftime(date_format)

            note = Note(id, title, clip, 'note', author,
                        literal_date, [], metadata)

            id += 1

            append(note)

    return list_notes


if __name__ == '__main__':
    clippings = get_clippings_from_path()
    with open('notes.json', 'w') as f:
        json = jsons.dumps(clippings)
        f.write(json)
    print('Dumped correctly!')

