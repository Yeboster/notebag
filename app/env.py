from typing import Dict
import os


def _get_envs_from_file(path: str = './.env') -> Dict[str, str]:
    env = {}
    with open(path) as f:
        while True:
            line = f.readline()
            if line == '':
                break
            line = line.strip('\n').strip()
            if not line.startswith('#'):
                key, val = line.split('=')
                env[key] = val
    return env


def get_env(key: str) -> str:
    # If params are empty, get them from environment or from the .env file
    try:
        val = os.environ[key]
    except:
        env = _get_envs_from_file()
        val = env[key]

    assert val is not None

    return val
