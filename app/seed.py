from typing import *
import app.db as db
from pymongo.database import Database
import json


# TODO: Do not insert duplicates
def seed():
    client: Database = db.connect()

    print('[*] Getting notes collection')
    notes: Collection = db.get_or_create_collection(client, 'notes')

    print('[*] Open notes.json file')
    with open('./notes.json') as f:
        clippings: List[Any] = json.load(f)
        print('[*] Insert documents')
        notes.insert_many(clippings)

        how_many = notes.count_documents({})
        print('[+] Inserted: ', how_many)


def delete_docs_notes():
    client: Database = db.connect()
    notes: Collection = db.get_or_create_collection(client, 'notes')

    print('[*] Drop notes collection')
    notes.delete_many({})
    print('[+] Deleted all documents in notes')
