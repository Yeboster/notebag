from typing import *
from flask import Flask
from app import api

app = Flask(__name__)


@app.route('/')
def index():
    # Return elm app
    return "Main page"


if __name__ == '__main__':
    app.register_blueprint(api.bp)
    app.run()
