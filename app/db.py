from pymongo import MongoClient
from pymongo.database import Database, Collection
from .env import get_env


def get_or_create_collection(client: Database, name: str) -> Collection:
    collection: Collection = client.get_collection(
        name) if name in client.list_collection_names() else client.create_collection(name)

    return collection


def connect(host: str = None, username: str = None, password: str = None, db_name: str = None) -> Database:
    """ Connect to MongoDB database. If the credentials are omitted, it will try to get them from the environment or the .env file """
    # If params are empty, get them from environment or from the .env file
    if host is None:
        host = get_env('DB_HOST')
    assert host is not None

    if db_name is None:
        db_name = get_env('DB_NAME')
    assert db_name is not None

    if username is None:
        username = get_env('DB_USER')
    assert username is not None

    if password is None:
        password = get_env('DB_PASSWORD')
    assert password is not None

    conn: MongoClient = MongoClient(
        f'mongodb://{username}:{password}@{host}:27017')

    db: Database = conn.get_database(db_name)

    assert db is not None

    return db
