from typing import *
from functools import wraps
from .db import connect, get_or_create_collection
from bson import json_util
from pymongo.database import Database, Collection
from pymongo.cursor import Cursor
from flask import Blueprint, jsonify, Response
from math import ceil


bp = Blueprint('api', __name__, url_prefix='/api')

PAGE_SIZE = 20


def get_collection(name: str):
    def decorator(f):
        @wraps(f)
        def decorated_func(*args, **kwargs) -> Collection:
            client: Database = connect()
            collection: Collection[Any] = get_or_create_collection(
                client, name)

            return f(collection, *args, **kwargs)
        return decorated_func
    return decorator


def cursor_to_json(cursor: Cursor) -> Response:
    json = json_util.dumps(cursor)

    return Response(json, mimetype="application/json", content_type="application/json")


@bp.route('notes/pages')
@get_collection('notes')
def pages(notes: Collection):

    pages_num = notes.count_documents({}) / PAGE_SIZE
    pages_num = ceil(pages_num)

    return jsonify(pages_count=pages_num)


@bp.route('notes/pages/<int:page>')
@get_collection('notes')
def notes_of(notes: Collection, page: int):
    if page > 0:
        page -= 1
        cursor: Cursor = notes.find({}, projection={'_id': 0})[
            PAGE_SIZE*page:PAGE_SIZE*(page+1)]
        return cursor_to_json(cursor)
    else:
        return jsonify(error='Pages starts from 1')


@bp.route('notes/<int:number>')
@get_collection('notes')
def note(notes: Collection, number: int):
    cursor: Cursor = notes.find_one({'id': number}, projection={'_id': 0})
    return cursor_to_json(cursor)
